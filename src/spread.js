/*
  //////////////////////
  // Promise.spread  //
  ////////////////////
*/

const BlueBird = require("bluebird");
const fs = BlueBird.promisifyAll(require('fs'));

BlueBird.all([
  fs.readFileAsync('file1.txt'),
  fs.readFileAsync("file2.txt")
]).spread(function(file1text, file2text) {
  if (file1text === file2text) {
      console.log("Bluebird: files are equal");
  }
  else {
      console.log("Bluebird: files are not equal");
  }
});

// Bluebird using ES6

BlueBird.delay(500).then(function() {
  return [fs.readFileAsync("file1.txt"),
          fs.readFileAsync("file2.txt")] ;
}).all().then(function([file1text, file2text]) {
   if (file1text === file2text) {
       console.log("Bluebird ES6: files are equal");
   }
   else {
       console.log("Bluebird ES6: files are not equal");
   }
});

Promise.all([
  fs.readFileAsync('file1.txt'),
  fs.readFileAsync("file2.txt")
]).then((text) => {
  if (text[1] === text[2]) {
    console.log("Promise: files are equal");
  }
  else {
    console.log("Promise: files are not equal");
  }
}).catch((err) => {
  throw err;
});