/**
 * /////////////////////
 * ///    .catch     ///
 * /////////////////////
 */

 const Promise = require("bluebird");
 const request = Promise.promisify(require("request"));
 
 function ClientError(e) {
     return e.code >= 400 && e.code < 500;
 }
 
 request("https://www.google.com").then(function(contents) {
     console.log('Contents: ',contents);
 }).catch(ClientError, function(e) {
    //A client error like 400 Bad Request happened
    console.log(e);
 });


