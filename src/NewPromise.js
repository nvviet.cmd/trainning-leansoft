
/**
* ///////////////////
* // New Promise  //
* //////////////////
*/
const XMLHttpRequest = require('xhr2');

function ajaxGetAsync(url) {
  return new Promise(function(resolve, reject) {
    const xhr = new XMLHttpRequest;
    xhr.addEventListener('error', reject);
    xhr.addEventListener('load', resolve);
    xhr.open("GET", url);
    xhr.send(null);
  });
}
const url = 'https://google.com/search';

ajaxGetAsync(url)
  .then((res) => console.log('ok'))
  .catch((err) => console.log('err'))
