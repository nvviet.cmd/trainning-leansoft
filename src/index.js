
// const myCar = {
//   color: 'blue',
//   type: 'sedan',
//   doors: 4
// }

// class Car {
//   constructor(color, type, doors) {
//     this.color = color;
//     this.type = type;
//     this.doors = doors;
//   }
// }

// const myCar = new Car('blue', 'sedan', 4)

// console.log(myCar);

/**
 * //////////////////////
 * ///  CALLBACK     ///
 * /////////////////////
 */

/**
 * //////////////////////
 * ///    Promise    ///
 * /////////////////////
 */

/**
 * //////////////////////
 * ///  Async/await  ///
 * /////////////////////
 */



/*
  //////////////////////
  // Promise.join  //
  ////////////////////
*/
function say(text) {
  return text;
}

// const bbJoin = BlueBird.join(
//   readFileAsync('file1.txt'), 
//   readFileAsync('file2.txt'), 
//   say('Hi viet'), 
//   function(text1, text2, text3) {
//     console.log(text1);
//     console.log(text2);
//     console.log(text3);
// });

/*
  //////////////////////
  // Promise.try///  //
  ////////////////////
*/

function getId(id) {
  return BlueBird.try(() => {
    if (typeof id !== 'number') {
      throw new Error('id must be a number');
    }
    return id;
  });
}

// getId('a')
//   .then((value) => {
//     console.log('Number: ',value);
//   })
//   .catch((err) => {
//     console.log(err);
//   })

/**
 * /////////////////////
 * /// Promise.method ///
 * /////////////////////
 */

/**
 * /////////////////////
 * /// PromisifyAll  ///
 * /////////////////////
 */

// chuyen tat ca ham trong fs sang dang Promise
const rFileAsync = BlueBird.promisify(fs.readFile); 

rFileAsync('file1.txt')
  .then((text) => console.log(text.toString()))
  .catch((err) => console.log(err))


