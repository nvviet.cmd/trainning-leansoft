# Bluebird
1. New Promise
- Tạo ra một promise mới. Hàm được truyền sẽ nhận hàm resolve và reject 

```
new Promise(function(function resolve, function reject) resolver) -> Promise
```

###### Example
```
  const XMLHttpRequest = require('xhr2');

  function ajaxGetAsync(url) {
    return new Promise(function(resolve, reject) {
      const xhr = new XMLHttpRequest;
      xhr.addEventListener('error', reject);
      xhr.addEventListener('load', resolve);
      xhr.open("GET", url);
      xhr.send(null);
    });
  }
  const url = 'https://google.com/search';

  ajaxGetAsync(url)
    .then((res) => console.log('ok'))
    .catch((err) => console.log('err'))
```

2. .then

```
.then(
    [function(any value) fulfilledHandler],
    [function(any error) rejectedHandler]
) -> Promise
```

3. .spread
- Như .then, nhưng các giá trị fullfill phải là một mảng

```
.spread(
    [function(any values...) fulfilledHandler]
) -> Promise
```

4. .catch
- .catch là một hàm thuận tiện cho việc xử lý lỗi trong promise chain.
- Có 2 biến thể:
+ Catch-all: 

  ```
    .catch(function(any error) handler) -> Promise
  ```

+ Filtered Catch:

  ```
    .catch(
        class ErrorClass|function(any error)|Object predicate...,
        function(any error) handler
    ) -> Promise
  ```

5. .error
- Giống với .catch, nó chỉ bắt các lỗi hoạt động

```
.error([function(any error) rejectedHandler]) -> Promise
```
